import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName

import scala.annotation.meta.field

case class Result(@(SerializedName @field)("hist_film") histFilm: Array[Int],
                  @(SerializedName @field)("hist_all") histAll: Array[Int]) {
  def toJSON: String = new GsonBuilder().setPrettyPrinting().create().toJson(this)

}