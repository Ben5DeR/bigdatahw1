import java.io._

import scala.io.Source

object RatingsCounter {

  def main(args: Array[String]) {

    val filmId = if (args.length == 1) args(0).toInt else 1366

    val path = if (args.length == 2) args(1) else "ml-100k/u.data"

    val fileReader = Source.fromFile(path)
    val df = fileReader
      .getLines
      .map(_.split("\t").slice(1, 3))
      .map(x => Seq(x(0).toInt, x(1).toInt)).duplicate

    val histFilmArray = df
      ._1
      .filter(_.head == filmId)
      .countRating


    val histAllArray = df
      ._2
      .countRating
    val result = Result(histFilmArray, histAllArray)

    writeFile("result.json", result.toJSON)

    fileReader.close()
  }

  def writeFile(filename: String, str: String): Unit = {
    val file = new File(filename)
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(str)
    bw.close()
  }

  implicit class ExtendedIntSeqIterator(df: Iterator[Seq[Int]]) {
    def countRating: Array[Int] =
      df
        .toSeq
        .map(_ (1))
        .groupBy(x => x)
        .mapValues(_.size)
        .fillMissingRating
  }

  implicit class ExtendedIntIntMap(seq: Map[Int, Int]) {
    def fillMissingRating: Array[Int] =
      (1 to 5)
        .map(x => seq.getOrElse(x, 0))
        .toArray
  }

}